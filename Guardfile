# A sample Guardfile
# More info at https://github.com/guard/guard#readme

## Uncomment and set this to only include directories you want to watch
# directories %w(app lib config test spec features) \
#  .select{|d| Dir.exists?(d) ? d : UI.warning("Directory #{d} does not exist")}

## Note: if you are using the `directories` clause above and you are not
## watching the project directory ('.'), then you will want to move
## the Guardfile to a watched dir and symlink it back, e.g.
#
#  $ mkdir config
#  $ mv Guardfile config/
#  $ ln -s config/Guardfile .
#
# and, you'll have to watch "config/Guardfile" instead of "Guardfile"

guard :minitest, spring: "bin/rails test", all_on_start: false do
  reg_test = %r{^test/(.*)\/?test_(.*)\.rb$}
  reg_test_helper = %r{^test/test_helper\.rb$}
  reg_model = %r{^app/models/(.*?)\.rb$}
  reg_controller = %r{^app/controllers/(.*?)_controller\.rb$}
  reg_view = %r{^app/views/([^/]*?).*\.html\.(slim|erb)$}
  reg_helper = %r{^app/helpers/(.*?)_helper\.rb$}
  reg_view_user = %r{app/views/users/*}

  watch(reg_test)
  watch(reg_test_helper) { 'test' }
  watch('config/routes.rb') { integration_tests }
  watch(reg_model) do |matches|
    "test/models/#{matches[1]}_test.rb"
  end
  watch(reg_controller) do |matches|
    resource_tests(matches[1])
  end
  watch(reg_view) do |matches|
    ["test/controllers/#{matches[1]}_controller_test.rb"] + integration_test(matches[1])
  end
  watch(reg_helper) do |matches|
    integration_tests(matches[1])
  end
  watch('app/views/layouts/application.html.slim') do
    'test/integration/site_layout_test.rb'
  end
  watch('app/helpers/sessions_helper.rb') do
    integration_tests << 'test/helpers/sessions_helper_test.rb'
  end
  watch('app/controllers/session_controller.rb') do
    ['test/controllers/sessions_controller_test.rb',
     'test/integration/users_login_test.rb']
  end
  watch('app/controllers/account_activations_controller.rb') do
    'test/integration/users_signup_test.rb'
  end
  watch(reg_view_user) do
    resource_tests('users') + ['test/integration/microposts_interface_test.rb']
  end


  # with Minitest::Spec
  # watch(%r{^spec/(.*)_spec\.rb$})
  # watch(%r{^lib/(.+)\.rb$})         { |m| "spec/#{m[1]}_spec.rb" }
  # watch(%r{^spec/spec_helper\.rb$}) { 'spec' }

  # Rails 4
  # watch(%r{^app/(.+)\.rb$})                               { |m| "test/#{m[1]}_test.rb" }
  # watch(%r{^app/controllers/application_controller\.rb$}) { 'test/controllers' }
  # watch(%r{^app/controllers/(.+)_controller\.rb$})        { |m| "test/integration/#{m[1]}_test.rb" }
  # watch(%r{^app/views/(.+)_mailer/.+})                   { |m| "test/mailers/#{m[1]}_mailer_test.rb" }
  # watch(%r{^lib/(.+)\.rb$})                               { |m| "test/lib/#{m[1]}_test.rb" }
  # watch(%r{^test/.+_test\.rb$})
  # watch(%r{^test/test_helper\.rb$}) { 'test' }

  # Rails < 4
  # watch(%r{^app/controllers/(.*)\.rb$}) { |m| "test/functional/#{m[1]}_test.rb" }
  # watch(%r{^app/helpers/(.*)\.rb$})     { |m| "test/helpers/#{m[1]}_test.rb" }
  # watch(%r{^app/models/(.*)\.rb$})      { |m| "test/unit/#{m[1]}_test.rb" }
end

# 与えられたリソースに対応する統合テストを返す
def integration_tests(resource = :all)
  if resource == :all
    Dir["test/integration/*"]
  else
    Dir["test/integration/#{resource}_*.rb"]
  end
end

# 与えられたリソースに対応するコントローラーのテストを返す  
def controller_test(resource)
  "test/controllers/#{resource}_controller_test.rb"
end

# 与えられたリソースに対応する全てのテストを返す  
def resource_tests(resource)
  integration_tests(resource) << controller_test(resource)
end
