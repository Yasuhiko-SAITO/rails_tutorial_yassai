require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(
      name: "Example User",
      email: "user@example.com",
      password: "password",
      password_confirmation: "password"
    )
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = " "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = "  "
    assert_not @user.valid?
  end

  test "name shouldn't be too long" do
    max_length = 50
    @user.name = "a" * (max_length + 1)
    assert_not @user.valid?
  end

  test "email shouldn't be too long" do
    max_length = 255
    domain = "@example.com"
    @user.email = "a" * (max_length + 1 - domain.length) + domain
    assert_not @user.valid?
  end

  test "email validation should accept valid email" do
    valid_email_list = %w(
    user@example.com
    USER@foo.COM
    A_US-ER@foo.bar.org
    first.last@foo.jp
    alice+bob@baz.cn
    )
    valid_email_list.each do |valid_email|
      @user.email = valid_email
      assert @user.valid?, "#{valid_email} had to be accepted, but it was failed!!"
    end
  end

  test "email validation should reject invalid email" do
    invalid_email_list = %w(
    user@example,com
    user.org
    user@example.
    foo@bar_baz.com
    foo@bar+baz.com
    foo@bar..com
    )
    invalid_email_list.each do |invalid_email|
      @user.email = invalid_email
      assert_not @user.valid?, "#{invalid_email} had to be rejected, but it was failed!!"
    end
  end

  test "email address should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email address should be saved as lower-case" do
    mixed_email_case = "Foo@bAr.coM"
    @user.email = mixed_email_case
    @user.save
    assert_equal mixed_email_case.downcase, @user.reload.email
  end

  test "password should be present" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end

  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference "Micropost.count", -1 do
      @user.destroy
    end
  end

  test "should follow and unfollow a user" do
    michael = users(:michael)
    archer = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert michael.following?(archer)
    assert archer.followers.include?(michael)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end

  test "feed should have the right posts" do
    michael = users(:michael)
    archer = users(:archer)
    lana = users(:lana)
    # フォローしているユーザーの投稿の確認
    lana.microposts.each do |post_following|
      assert michael.feed.include?(post_following)
    end
    # 自分自身の投稿の確認
    michael.microposts.each do |post_self|
      assert michael.feed.include?(post_self)
    end
    # フォローしてないユーザーの投稿の確認
    archer.microposts.each do |post_unfollowing|
      assert_not michael.feed.include?(post_unfollowing)
    end
  end
end
