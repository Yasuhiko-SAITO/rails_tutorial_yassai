require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:michael)
  end

  test "password resets" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    # メールアドレスが無効の場合
    post password_resets_path, params: { password_reset: { email: ""} }
    assert_not flash.empty?
    assert_template 'password_resets/new'
    # メールアドレスが有効の場合
    post password_resets_path, params: { password_reset: { email: @user.email } }
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.empty?
    assert_redirected_to root_path

    # パスワード再設定フォームのテスト
    user = assigns(:user)
    # メールアドレスが無効の場合
    get edit_password_reset_path(user.reset_token, email: "")
    assert_redirected_to root_path
    # 無効なユーザーの場合
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_path
    user.toggle!(:activated)
    # トークンが無効の場合
    get edit_password_reset_path("wrong token", email: user.email)
    assert_redirected_to root_path
    # 全て有効の場合
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit'
    assert_select "input[name=email][type=hidden][value=?]", user.email

    # 入力パスと確認パスが異なる場合
    patch password_reset_path(user.reset_token), params: { email: user.email, user: {
      password: "password1",
      password_confirmation: "password2"
    }}
    assert_select 'div#error_explanation'
    # パスワードが空の場合
    patch password_reset_path(user.reset_token), params: { email: user.email, user: {
      password: "",
      password_confirmation: ""
    }}
    assert_select 'div#error_explanation'
    # 全て有効の場合
    patch password_reset_path(user.reset_token), params: { email: user.email, user: {
      password: "password",
      password_confirmation: "password"
    }}
    assert_nil user.reload.reset_digest
    assert is_logged_in?
    assert_not flash.empty?
    assert_redirected_to user
  end

  test "expired token" do
    get new_password_reset_path
    post password_resets_path, params: { password_reset: { email: @user.email } }
    @user = assigns(:user)
    @user.update_attribute(:reset_sent_at, 3.hours.ago)
    patch password_reset_path(@user.reset_token), params: { email: @user.email, user: {
      password: "password",
      password_confirmation: "password"
    }}
    assert_response :redirect
    follow_redirect!
    assert_match /expired/i, response.body
  end
end
