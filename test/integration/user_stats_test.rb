require 'test_helper'

class UserStatsTest < ActionDispatch::IntegrationTest
  def setup
    @user_1 = users(:user_1)
    @user_2 = users(:user_2)
  end

  test "show stats in root path" do
    log_in_as(@user_1)
    get root_path
    assert_select 'strong#following', '0'
    assert_select 'strong#followers', '0'
    # フォロー数が増えたとき
    @user_1.follow(@user_2)
    get root_path
    assert_select 'strong#following', '1'
    # フォロワー数が増えたとき
    @user_2.follow(@user_1)
    get root_path
    assert_select 'strong#followers', '1'
  end
end
