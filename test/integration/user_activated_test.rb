require 'test_helper'

class UserActivatedTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @non_activated_user = users(:archer)
  end

  test 'index page displays users without non-activated' do
    @non_activated_user.update_attribute(:activated, false)
    log_in_as(@user)
    get users_path
    users = User.where(activated: true)
    assert_not_includes users, @non_activated_user
  end

  test 'non-activated user redirect to root' do
    @non_activated_user.update_attribute(:activated, false)
    get user_path(@non_activated_user)
    follow_redirect!
    assert_template 'static_page/home'
  end
end
